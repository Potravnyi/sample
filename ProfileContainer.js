import { connect } from "react-redux";

import ProfileView from "./ProfileView";
import {
  helperCalled,
  setCarFreeSeats,
  setCarNumber,
  setCarPhoto,
  setCostDriverMin,
  setCostDriverProposal,
  setCostPsngrMax,
  setCostPsngrProposal,
  setName,
  setPoiGeoCoords,
  setPoiName,
  setRegDate,
  setRoute,
  setSelfie,
  setTimeStartFrom,
  setTimeStartTo,
  toggleDayOfWeek,
  toggleRole
} from "./state/actions/actionsProfile";

const mapStateToProps = profile => ({ profile });

const mapDispatchToProps = {
  helperCalled,
  setCarFreeSeats,
  setCarNumber,
  setCarPhoto,
  setCostDriverMin,
  setCostDriverProposal,
  setCostPsngrMax,
  setCostPsngrProposal,
  setName,
  setPoiGeoCoords,
  setPoiName,
  setRegDate,
  setRoute,
  setSelfie,
  setTimeStartFrom,
  setTimeStartTo,
  toggleDayOfWeek,
  toggleRole
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileView);
