import React, { Component, Fragment } from "react";
import {
  Dimensions,
  Keyboard,
  ScrollView,
  StatusBar,
  Alert
} from "react-native";
import { Button, Badge, Overlay } from "react-native-elements";
import Constants from "expo-constants";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";

import { PASSENGER, DRIVER } from "../constants/roles";
import helper from "../helper/helper";
import PoiView from "./components/PoiView";
import HeaderCommon from "../components/HeaderCommon";
import RowWithPhotoView from "./components/RowWithPhotoView";
import RowWithInputView from "./components/RowWithInputView";
import RoleSelectorView from "./components/RoleSelectorView";
import RideScheduleView from "./components/RideScheduleView";
import styles from "./components/componentsStyles";
import StickyHeader from "./components/StickyHeader";
import PassengerCardView from "./components/PassengerCardView";
import RoutesView from "./components/RoutesView";
import { apiPath, headers } from "../../secrets/headers";

export default class ProfileView extends Component {
  constructor(props) {
    super(props);
    this.scrollViewRef = null;
    this.state = {
      isPoiMapVisible: new Array(props.profile.poi.length).fill(false),
      isRouteMapVisible: new Array(props.profile.routes.length).fill(false),
      isCardVisible: false,
      name: this.props.profile.name,
      poi: [{ name: "" }, { name: "" }]
    };
    this.dataLayout = {
      scrollPos: 0,
      focusedPos: 0
    };
    this.setFocusedPos = e => {
      const { pageY, locationY } = e.nativeEvent;
      this.dataLayout.focusedPos = pageY - locationY + 50;
    };
    this.getRoleLocal = () =>
      this.props.profile.role === PASSENGER ? "пассажир" : "водитель";
    this.onChangeName = name => {
      if (name === "" || name.slice(-1).match(/[a-zа-яіїєёґ ']/i))
        this.setState({ name });
    };
  }

  componentDidMount() {
    const requiredPermissions = ["CAMERA", "CAMERA_ROLL"];
    requiredPermissions.forEach(el => {
      this.getPermissionAsync(el);
    });
    this.keyboardListener = Keyboard.addListener(
      "keyboardDidShow",
      this.keyboardHandler.bind(this)
    );
  }
  componentDidUpdate(prevProps) {
    if (prevProps.profile !== this.props.profile) {
      this.setState(prevState => {
        return {
          ...prevState,
          name: this.props.profile.name,
          poi: this.props.profile.poi.map(poi => ({
            name: poi.name
          }))
        };
      });
      if (this.props.profile.helperCalls !== "completed") {
        clearTimeout(trainingSetTimeoutID);
        trainingSetTimeoutID = setTimeout(() => {
          trainHelper(this.props.profile.helperCalls, this.props.helperCalled);
        }, 1000);
      }
    }
  }
  componentWillUnmount() {
    this.keyboardListener.remove();
  }
  keyboardHandler(e) {
    if (
      this.state.isPoiMapVisible.some(it => it) ||
      this.state.isRouteMapVisible.some(it => it)
    )
      return;
    const keyboardHeight = e.endCoordinates.height;
    const inputPos = this.dataLayout.scrollPos + this.dataLayout.focusedPos;
    const freeWindowHeight = Dimensions.get("window").height - keyboardHeight;
    const scrollPos = inputPos - freeWindowHeight;
    this.scrollViewRef.scrollTo({ x: 0, y: scrollPos, animated: true });
  }
  getPermissionAsync = async permission => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions[permission]);
      if (status !== "granted") {
        alert(
          "Упс, для использования фотографий нужно разрешение на доступ к камере."
        );
      }
    }
  };
  _pickImage = () => {
    const config = {
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1]
    };
    return new Promise(resolve => {
      Alert.alert("Фото", "Выберите источник:", [
        {
          text: "Отменить",
          style: "cancel"
        },
        {
          text: "Файл",
          onPress: async () => {
            const result = await ImagePicker.launchImageLibraryAsync(config);
            if (!result.cancelled) resolve(result.uri);
          }
        },
        {
          text: "Камера",
          onPress: async () => {
            const result = await ImagePicker.launchCameraAsync(config);
            if (!result.cancelled) resolve(result.uri);
          }
        }
      ]);
    });
  };
  pickSelfieImage = async () => {
    const uri = await this._pickImage();
    this.props.setSelfie(uri);
  };
  pickCarImage = async () => {
    const uri = await this._pickImage();
    this.props.setCarPhoto(uri);
  };
  pickCarRegNumberImage = async () => {
    const uri = await this._pickImage();
    this.fetchGoogleVision(uri);
  };
  fetchGoogleVision = async file => {
    const url = "https://ridemate-club.cloudfunctions.net/api" + apiPath;
    const filename = file.split("/").pop();
    const ext = /\.(\w+)$/.exec(filename);
    const type = ext ? `image/${ext[1]}` : `image`;
    const data = new FormData();
    data.append("regplate", { uri: file, name: filename, type });
    const fetchInit = {
      method: "POST",
      headers: new Headers(headers),
      body: data
    };
    try {
      const response = await fetch(url, fetchInit);
      const responseBlob = await response.blob();
      const fileReader = new FileReader();
      fileReader.onload = async () => {
        const srtingBase64wHeader = fileReader.result;
        //  Replacing header: 'data:application/octet-stream'.length = 37
        const newHeader = `data:image/jpg;base64,`;
        const file = newHeader + srtingBase64wHeader.slice(37);
        this.props.setCarNumber(file);
      };
      fileReader.onabort = e => console.log("onabort", e);
      fileReader.readAsDataURL(responseBlob);
    } catch (error) {
      Alert.alert("Ошибка сети", "Пожалуйста повторите запрос.", [
        { text: "Ok" }
      ]);
    }
  };
  onChangeSeatsNumber = string => {
    const number = parseInt(string);
    if (string === "") this.props.setCarFreeSeats("");
    if (number > 0 && number < 10) {
      this.props.setCarFreeSeats(number);
      Keyboard.dismiss();
    }
  };

  render() {
    const role = this.props.profile.role;
    const sourceSelfieImg = this.props.profile.selfie
      ? { uri: this.props.profile.selfie }
      : require("./img/avatar/avatar.png");
    const sourceCarImg = this.props.profile.carPhoto
      ? { uri: this.props.profile.carPhoto }
      : require("./img/carAvatar/car_avatar.png");
    const sourceCarNumberImg = this.props.profile.carRegNumber
      ? { uri: this.props.profile.carRegNumber }
      : require("./img/regPlateNumberAvatar/carRegPlateAvatar.png");
    const completion = this.props.profile.completion[role];

    return (
      <Fragment>
        <StatusBar barStyle="light-content" />
        <HeaderCommon
          centerText={"Профиль"}
          onCenterPress={() => {
            helper({
              title: "Профиль",
              key: "profile",
              callback: () => {
                this.props.helperCalled("trainProfile");
              }
            });
          }}
          rightComponent={
            <Badge
              value={completion + "%"}
              status={
                completion === 0
                  ? "error"
                  : completion === 100
                  ? "success"
                  : "warning"
              }
            />
          }
        />

        <ScrollView
          contentContainerStyle={styles.container}
          ref={ref => {
            this.scrollViewRef = ref;
          }}
          scrollEventThrottle={50}
          stickyHeaderIndices={role === DRIVER ? [0, 5] : [0, 4]}
          onScroll={e => {
            this.dataLayout.scrollPos = e.nativeEvent.contentOffset.y;
          }}
        >
          <StickyHeader
            text="ПУБЛИЧНЫЙ"
            onPress={() => {
              helper({
                title: "ПУБЛИЧНЫЙ",
                key: "publicProfile",
                role,
                callback: () => {
                  this.props.helperCalled("trainPublicProfile");
                }
              });
            }}
          />
          <RowWithPhotoView
            onTextPress={() => helper({ title: "моё фото", key: "selfie" })}
            text={"моё фото"}
            onPhotoPress={this.pickSelfieImage.bind(this)}
            imgSource={sourceSelfieImg}
            isEmpty={!this.props.profile.selfie}
          />
          <RowWithInputView
            text={"меня зовут"}
            onTextPress={() => {
              helper({
                title: "меня зовут",
                key: "name",
                callback: () => {
                  this.props.helperCalled("trainName");
                }
              });
            }}
            inputProps={{
              containerStyle: { flex: 1 },
              placeholder: "имя, только буквы",
              onChangeText: this.onChangeName.bind(this),
              onBlur: () => {
                this.props.setName(this.state.name);
              },
              value: this.state.name,
              width: "30%",
              onToucStart: this.setFocusedPos
            }}
          />
          <RoleSelectorView
            role={role}
            text={"я"}
            onTextPress={() => helper({ title: "я", key: "role" })}
            toggleRole={this.props.toggleRole}
          />
          {role === PASSENGER ? null : (
            <Fragment>
              <RowWithPhotoView
                text={"фото машины"}
                onTextPress={() =>
                  helper({ title: "фото машины", key: "carPhoto" })
                }
                onPhotoPress={this.pickCarImage.bind(this)}
                imgSource={sourceCarImg}
                isEmpty={!this.props.profile.carPhoto}
              />

              <RowWithPhotoView
                text={"фото номера машины"}
                onTextPress={() =>
                  helper({
                    title: "фото номера машины",
                    key: "registrationPhoto"
                  })
                }
                onPhotoPress={this.pickCarRegNumberImage.bind(this)}
                imgSource={sourceCarNumberImg}
                isEmpty={!this.props.profile.carRegNumber}
                imgStyle={{ width: 100, height: 60 }}
                title="registrationPhoto"
              />

              <RowWithInputView
                text={"мест для попутчиков"}
                onTextPress={() =>
                  helper({ title: "мест для попутчиков", key: "psngrsSeats" })
                }
                inputProps={{
                  containerStyle: { width: 100 },
                  placeholder: "число",
                  onChangeText: this.onChangeSeatsNumber.bind(this),
                  value: this.props.profile.carFreeSeats
                    ? this.props.profile.carFreeSeats.toString()
                    : "",
                  keyboardType: "number-pad",
                  onTouchStart: this.setFocusedPos
                }}
              />
            </Fragment>
          )}
          <StickyHeader
            text="ПРИВАТНЫЙ"
            onPress={() =>
              helper({ title: "ПРИВАТНЫЙ", key: "privateProfile", role })
            }
          />
          <PoiView
            poi={[...this.props.profile.poi]}
            isPoiMapVisible={this.state.isPoiMapVisible}
            parentStatePoi={this.state.poi}
            parentSetState={this.setState.bind(this)}
            setPoiGeoCoords={this.props.setPoiGeoCoords}
            onBlurFunc={this.props.setPoiName}
            onTouchStartFunc={this.setFocusedPos}
          />
          <RideScheduleView
            rideSchedule={this.props.profile.rideSchedule}
            poi={this.props.profile.poi}
            role={this.props.profile.role}
            toggleDayOfWeek={this.props.toggleDayOfWeek}
            setTimeStartFrom={this.props.setTimeStartFrom}
            setTimeStartTo={this.props.setTimeStartTo}
            onTouchStartFunc={this.setFocusedPos}
            setCostPsngrProposal={this.props.setCostPsngrProposal}
            setCostPsngrMax={this.props.setCostPsngrMax}
            setCostDriverProposal={this.props.setCostDriverProposal}
            setCostDriverMin={this.props.setCostDriverMin}
          />

          {role === PASSENGER ? null : (
            <RoutesView
              routes={this.props.profile.routes}
              poi={this.props.profile.poi}
              setRoute={this.props.setRoute}
              isRouteMapVisible={this.state.isRouteMapVisible}
              parentSetState={this.setState.bind(this)}
            />
          )}

          <Button
            type="clear"
            title="Как выглядит Ваш профиль"
            buttonStyle={{
              alignSelf: "center",
              padding: 15
            }}
            onPress={() => this.setState({ isCardVisible: true })}
          />
          <Overlay isVisible={this.state.isCardVisible} fullScreen={true}>
            <PassengerCardView
              {...this.props.profile}
              onPress={() => this.setState({ isCardVisible: false })}
            />
          </Overlay>
        </ScrollView>
      </Fragment>
    );
  }
}

let trainingSetTimeoutID = null;
const trainHelper = (helperCalls = [], trainingOver, clear) => {
  if (!helperCalls.includes("trainProfile"))
    helper({
      title: "Помощник",
      key: "trainProfile"
    });
  else if (!helperCalls.includes("trainPublicProfile"))
    helper({
      title: "Помощник",
      key: "trainPublicProfile"
    });
  else if (!helperCalls.includes("trainName"))
    helper({
      title: "Помощник",
      key: "trainName"
    });
  else {
    helper({
      title: "Помощник",
      key: "trainCompleted"
    });
    trainingOver("completed");
  }
};
